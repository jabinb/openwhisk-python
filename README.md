## Intro
This Docker image is the base image of OpenWhisk action to run [event-parser](https://gitlab.com/simplywallst/backend/event-parser). It contains all of the project dependencies.

## Build & Deploy
The image contains all dependencies and the models from [event-parser](https://gitlab.com/simplywallst/backend/event-parser) repository. We are including the models into the Docker image to avoid 48 MB limit of IBM Cloud functions.

To build the Docker image, you'd need an SSH key that have read access to event-parser repository. The SSH key file is assumed to be located in ~/.ssh/id_rsa. You may change the location by editing the Makefile.

To build and push the image into GitLab containers repository simply use the following command:

```bash
make push
```

## Testing
During the build process `test.sh` file is copied into the image that perform a smoke test. It ensures all dependencies are 

