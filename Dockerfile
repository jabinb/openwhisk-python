FROM ibmfunctions/action-python-v3.7 as models
ARG SSH_KEY

env IBMCLOUD_ANALYTICS=false
env IBMCLOUD_VERSION_CHECK=false
env IBMCLOUD_COLOR=false

RUN mkdir -p /root/.ssh/ && \
    echo "$SSH_KEY" > /root/.ssh/id_rsa && \
    chmod 400 /root/.ssh/id_rsa && \
    ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts && \
    git clone git@gitlab.com:simplywallst/backend/event-parser.git

# Start the 2nd stage build
FROM ibmfunctions/action-python-v3.7
COPY --from=models /event-parser/spacy_models/ /spacy_models

RUN (curl -fsSL https://clis.cloud.ibm.com/install/linux | sh) && \
ln -s /usr/local/bin/ibmcloud /usr/local/bin/ic && \
ic api https://cloud.ibm.com && \
wget -qO - http://packages.confluent.io/deb/4.0/archive.key | apt-key add - && \
apt-get update && apt-get install software-properties-common -y && \
add-apt-repository "deb [arch=amd64] http://packages.confluent.io/deb/4.0 stable main" && \
apt-get update && apt-get install zip -y && \
apt-get remove software-properties-common -y && \
apt-get autoremove -y && \
apt-get clean && \
apt-get autoclean && \
rm -rf /var/lib/apt/lists/* && \
rm -rf /usr/share/man/?? && \
rm -rf /usr/share/man/??_* && \
rm -rf /var/cache/* && \
rm -rf /tmp/*



# These packages are required by kafka, spacy, and a few other libraries
RUN pip install pip --upgrade && \
    pip install spacy==2.0.15 numpy python-levenshtein python-dotenv datadog python-json-logger aiohttp requests click

# Smoke test the container structure!
COPY test.sh /root/test.sh
RUN chmod +x /root/test.sh && \
    /root/test.sh && \
    rm /root/test.sh