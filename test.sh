#!/usr/bin/env bash

set -aux

# Ensure system dependencies exists
which gcc
which python

# Ensure Python dependencies exists
dependencies=(spacy numpy python-levenshtein python-dotenv datadog python-json-logger aiohttp requests click)
for d in ${dependencies[@]}; do
    python -c "from importlib.util import find_spec; exit(0) if find_spec('$d') else exit(1)"
done

# Ensure models exists
ls /spacy_models