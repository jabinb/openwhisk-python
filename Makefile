IMAGE_NAME="registry.gitlab.com/jabinb/openwhisk-python:latest"

# Cleaning up docker from all caches, containers, and images.
# Useful for when docker is not functioning properly!
clean_docker:
	-docker kill $(docker ps -q)
	-docker rm $(docker ps --filter=status=exited --filter=status=created -q)
	-docker rmi $(docker images -a -q)
	-docker system prune

# Build the image locally
build:
	docker build --no-cache -t=${IMAGE_NAME} --build-arg SSH_KEY="$$(cat ~/.ssh/id_rsa)"  .

# Push the image into Gitlab registry
push:
	docker build --no-cache -t=${IMAGE_NAME} --build-arg SSH_KEY="$$(cat ~/.ssh/id_rsa)"  .
	docker login registry.gitlab.com
	docker push ${IMAGE_NAME}
